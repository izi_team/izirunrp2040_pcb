EESchema Schematic File Version 5
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title "IZIRUNF0"
Date "2020-06-21"
Rev "rev A"
Comp "IZITRON"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
$Comp
L izi-connectors:M_2 U4
U 1 1 5EEF272B
P 6000 3625
F 0 "U4" H 6000 5611 59  0000 C CIB
F 1 "M 2" H 6000 5522 39  0000 C CIB
F 2 "MyFootPrint:M.2_PCB" H 6000 3625 39  0001 C CIB
F 3 "" H 6000 3625 39  0001 C CIB
F 4 "2199119-4" H 6000 5449 39  0000 C CIB "REF"
F 5 "571-2199119-4" H 6000 3625 39  0001 C CIB "MOUSER"
F 6 "2377468" H 6000 3625 39  0001 C CIB "FARNELL"
F 7 "NC(only footprint)" H 6000 3625 39  0001 C CIB "ASSAMBLED"
	1    6000 3625
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR036
U 1 1 5EF7E053
P 4860 1975
F 0 "#PWR036" H 4860 1825 59  0001 C CIB
F 1 "+5V" H 4875 2148 39  0000 C CIB
F 2 "" H 4860 1975 39  0001 C CIB
F 3 "" H 4860 1975 39  0001 C CIB
	1    4860 1975
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5600 1925 4930 1925
Wire Wire Line
	4930 1925 4930 1975
Wire Wire Line
	4930 2025 5600 2025
Wire Wire Line
	4860 1975 4930 1975
Connection ~ 4930 1975
Wire Wire Line
	4930 1975 4930 2025
Text HLabel 4930 2125 0    50   Output ~ 0
USART1_TX
Text HLabel 4930 2225 0    50   Input ~ 0
USART1_RX
Wire Wire Line
	5600 2125 4930 2125
Wire Wire Line
	4930 2225 5600 2225
Text HLabel 4930 2325 0    50   Output ~ 0
I2C1_SCL
Text HLabel 4930 2425 0    50   BiDi ~ 0
I2C1_SDA
Wire Wire Line
	4930 2325 5600 2325
Wire Wire Line
	4930 2425 5600 2425
Text HLabel 4930 2525 0    50   Output ~ 0
SPI1_SCK
Wire Wire Line
	5600 2525 4930 2525
Text HLabel 4930 2625 0    50   Input ~ 0
SPI1_MISO
Wire Wire Line
	4930 2625 5600 2625
Text HLabel 4930 2725 0    50   Output ~ 0
SPI1_MOSI
Wire Wire Line
	4930 2725 5600 2725
Text HLabel 4930 2825 0    50   Output ~ 0
SPI1_CS0
Text HLabel 4930 2925 0    50   Output ~ 0
SPI1_CS1
Text HLabel 4930 3025 0    50   Output ~ 0
SPI1_CS2
Wire Wire Line
	5600 2825 4930 2825
Wire Wire Line
	4930 2925 5600 2925
Wire Wire Line
	5600 3025 4930 3025
Text HLabel 4930 3225 0    50   Input ~ 0
ADC0
Text HLabel 4930 3325 0    50   Input ~ 0
ADC1
Wire Wire Line
	4930 3225 5600 3225
Wire Wire Line
	4930 3325 5600 3325
Text HLabel 4925 3425 0    50   Output ~ 0
PWM0
Wire Wire Line
	4925 3425 5600 3425
Text HLabel 4925 3525 0    50   Output ~ 0
PWM1
Text HLabel 4925 3625 0    50   Output ~ 0
PWM2
Text HLabel 4925 3725 0    50   Output ~ 0
PWM3
Text HLabel 4925 3825 0    50   Output ~ 0
PWM4
Text HLabel 4925 3925 0    50   Output ~ 0
PWM5
Wire Wire Line
	4925 3525 5600 3525
Wire Wire Line
	5600 3625 4925 3625
Wire Wire Line
	4925 3725 5600 3725
Wire Wire Line
	4925 3825 5600 3825
Wire Wire Line
	4925 3925 5600 3925
Text HLabel 4925 4025 0    50   Output ~ 0
IO0
Text HLabel 4925 4125 0    50   Output ~ 0
IO1
Wire Wire Line
	4925 4025 5600 4025
Wire Wire Line
	5600 4125 4925 4125
NoConn ~ 5600 4225
NoConn ~ 5600 4325
NoConn ~ 5600 4425
NoConn ~ 5600 4525
NoConn ~ 5600 4625
NoConn ~ 5600 4725
NoConn ~ 5600 4825
NoConn ~ 5600 4925
NoConn ~ 5600 5025
NoConn ~ 5600 5125
NoConn ~ 5600 5225
NoConn ~ 5600 5325
$Comp
L power:GND #PWR037
U 1 1 5F071637
P 7190 2025
F 0 "#PWR037" H 7190 1775 59  0001 C CIB
F 1 "GND" H 7195 1880 39  0000 C CIB
F 2 "" H 7190 2025 39  0001 C CIB
F 3 "" H 7190 2025 39  0001 C CIB
	1    7190 2025
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6400 1975 7065 1975
Wire Wire Line
	7065 1975 7065 2025
Wire Wire Line
	7065 2075 6400 2075
Wire Wire Line
	7190 2025 7065 2025
Connection ~ 7065 2025
Wire Wire Line
	7065 2025 7065 2075
Text HLabel 7065 2175 2    50   Input ~ 0
NRST
Wire Wire Line
	6400 2175 7065 2175
Text HLabel 7065 2275 2    50   Input ~ 0
BOOT
Wire Wire Line
	6400 2275 7065 2275
Text HLabel 7065 2375 2    50   Input ~ 0
WKUP
Wire Wire Line
	6400 2375 7065 2375
Text HLabel 7065 2475 2    50   UnSpc ~ 0
CAN_H
Text HLabel 7065 2575 2    50   UnSpc ~ 0
CAN_L
Wire Wire Line
	6400 2475 7065 2475
Wire Wire Line
	7065 2575 6400 2575
NoConn ~ 6400 2675
NoConn ~ 6400 2775
NoConn ~ 6400 2875
NoConn ~ 6400 2975
NoConn ~ 6400 3175
NoConn ~ 6400 3275
NoConn ~ 6400 3375
NoConn ~ 6400 3475
NoConn ~ 6400 3575
NoConn ~ 6400 3675
NoConn ~ 6400 3775
NoConn ~ 6400 3875
NoConn ~ 6400 3975
NoConn ~ 6400 4075
NoConn ~ 6400 4175
NoConn ~ 6400 4275
NoConn ~ 6400 4375
NoConn ~ 6400 4475
NoConn ~ 6400 4675
NoConn ~ 6400 4575
NoConn ~ 6400 4775
NoConn ~ 6400 4875
NoConn ~ 6400 4975
NoConn ~ 6400 5075
NoConn ~ 6400 5175
NoConn ~ 6400 5275
Text Notes 6715 810  2    118  Italic 24
M2 CONNECTOR
$EndSCHEMATC
